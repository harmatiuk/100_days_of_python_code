student_heights = input("Please, tell me the student heights: \n").split()

total_height       = 0
number_of_students = 0

for student_height in student_heights:
    total_height       += int(student_height)
    number_of_students += 1

average_height = round(total_height / number_of_students, 2)

print(f"total height       = {total_height}")
print(f"number of students = {number_of_students}")
print(f"average height     = {average_height}")