# Input a list of student scores
student_scores = input("Please tell me the students score:\n").split()

highest_score = 0

for student_score in student_scores:
    student_score_int = int(student_score)
    if student_score_int > highest_score:
        highest_score = student_score_int
    
print(f"The highest score in the class is: {highest_score}")