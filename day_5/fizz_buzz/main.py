_fizz = lambda x: x % 3 == 0 
_buzz = lambda x: x % 5 == 0

for number in range(0,101):
    fizz = _fizz(number)
    buzz = _buzz(number)
    
    if fizz and buzz:
        print("FizzBuzz")
    elif fizz:
        print("Fizz")
    elif buzz:
        print("Buzz")
    else:
        print(number)
