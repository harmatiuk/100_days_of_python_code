import time

# Medindo tempo para a função lambda
start_time_lambda = time.time()
_fizz = lambda x: x % 3 == 0 
_buzz = lambda x: x % 5 == 0

for number in range(0,101):
    fizz = _fizz(number)
    buzz = _buzz(number)
    
    if fizz and buzz:
        print("FizzBuzz")
    elif fizz:
        print("Fizz")
    elif buzz:
        print("Buzz")
    else:
        print(number)

end_time_lambda = time.time()

# Medindo tempo para a abordagem convencional
start_time_convencional = time.time()
for number in range(0,101):
    if number % 3 == 0 and number % 5 == 0:
        print("FizzBuzz")
    elif number % 3 == 0:
        print("Fizz")
    elif  number % 5 == 0:
        print("Buzz")
    else:
        print(number)
end_time_convencional = time.time()

# Calculando os tempos de execução
tempo_lambda = end_time_lambda - start_time_lambda
tempo_convencional = end_time_convencional - start_time_convencional

print("Tempo de execução usando função lambda:", tempo_lambda)
print("Tempo de execução usando abordagem convencional:", tempo_convencional)
