import random
from start import start

data = start()

letters = data.get("letters")
symbols = data.get("symbols")
numbers = data.get("numbers")

nr_letters = data.get("nr_letters")
nr_symbols = data.get("nr_symbols")
nr_numbers = data.get("nr_numbers")

pwd = ''

for x in range(1, nr_letters + 1):
    pwd += random.choice(letters)

for y in range(1, nr_symbols + 1):
    pwd += random.choice(symbols)

for z in range(1, nr_numbers + 1):
    pwd += random.choice(numbers)
    
    
print(pwd)
