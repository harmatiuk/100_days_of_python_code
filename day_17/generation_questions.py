import requests
from question_model import Question

class GenerateQuestionsFromApi():
    
    def __init__(self) -> None:
        self.main_link  = "https://opentdb.com"
        self.category_to_play = None
        self.number_of_questions_to_play = None
        self.category_options = None
    
    
    def response(self, endpoint:str) -> dict:
        """
        Faz uma requisição GET para o endpoint da API e retorna a resposta como um dicionário.

        Args:
            endpoint: O endpoint da API a ser acessado.

        Returns:
            Um dicionário contendo a resposta da API em formato JSON.
            Retorna um exceptions em caso de erro.
        """
        
        try:
            response = requests.get(f"{self.main_link}{endpoint}")
            response.raise_for_status()
            
            return response.json()
        
        except requests.exceptions.RequestException as e:
            print(f"Ocorreu o seguinte erro ao chamar a API: {e}")
            
    
    def get_session_token(self) -> str:
        """
        Gera um novo token de sessão da API.

        Returns:
            O token de sessão como uma string. 
            Retorna uma string vazia em caso de erro.
        """
        
        response = self.response(endpoint="/api_token.php?command=request")
        
        if 'token' in response:
            return response['token']    
        else:
            raise KeyError("Erro: Chave 'token' não encontrada na respota da API.")
    
    def get_available_categories(self) -> dict:
        """
        Obtém as categorias disponíveis na API Open Trivia DB.

        Retorna um dicionário onde as chaves são números sequenciais 
        e os valores são dicionários contendo informações sobre cada categoria, 
        incluindo o 'api_id' e o 'name'. 

        As categorias são armazenadas em cache (em `self.category_options`) 
        após a primeira chamada para evitar requisições desnecessárias à API.

        Returns:
            dict: Um dicionário formatado com as categorias disponíveis.
        """

        if not self.category_options:
            response = self.response(endpoint="/api_category.php")
            categories = sorted(response['trivia_categories'], key=lambda item: item['name'])
            self.category_options = {n: {"api_id": category['id'], "name": category['name']} for n, category in enumerate(categories, start=1)}
        
        return self.category_options
    
    def display_available_categories(self) -> None:
            """
            Exibe as categorias disponíveis para o usuário no console.
            """

            categories = self.get_available_categories() 
            string_display = "\n".join(f"{key} - {value['name']}" for key, value in categories.items())
            print(string_display)
    
    def get_category_id(self) -> int:
           """
           Retorna o ID da categoria escolhida pelo usuário.
           Returns:
               int: O ID da categoria.
           Raises:
               KeyError: Se a categoria escolhida não for encontrada.
           """
           
           if self.category_to_play in self.category_options:
               return self.category_options[self.category_to_play]['api_id']
           else:
               raise KeyError("Categoria escolhida não encontrada.")

    def get_questions(self):
            """
            Obtém as perguntas da API Open Trivia DB.

            Utiliza a categoria, número de perguntas e token de sessão 
            definidos nos atributos da classe para construir a requisição à API. 

            Returns:
                dict: Um dicionário contendo a resposta da API com as perguntas, 
                      ou um dicionário vazio em caso de erro. 
            """

            response = self.response(
                f"/api.php?amount={self.number_of_questions_to_play}"
                f"&category={self.get_category_id()}"
                f"&type=boolean&token={self.get_session_token()}"
            )

            return response
    
    def create_question_bank(self):
        """
        Cria uma lista de objetos `Question` a partir das perguntas da API.

        Obtém as perguntas da API usando `self.get_questions()` e as converte 
        em objetos `Question`, armazenando-os em uma lista.

        Returns:
            list: Uma lista de objetos `Question`, ou uma lista vazia 
                  se não houver perguntas disponíveis.
        """
        
        question_bank = []
        questions = self.get_questions()
        if questions:
            for item in questions['results']:
                question = item['question']
                answer   = item['correct_answer']
                question_bank.append(Question(question, answer))
            
        return question_bank
    
    def questions_generator(self) -> None:
        """
        Gerencia a interação com o usuário e a geração do banco de perguntas.

        Exibe as categorias disponíveis, solicita ao usuário 
        que escolha uma categoria e o número de perguntas desejado. 
        Em seguida, gera o banco de perguntas usando a categoria e o 
        número de perguntas fornecidos.

        Returns:
            list: Uma lista de objetos `Question` representando as perguntas 
                  do quiz, ou uma lista vazia se não houver perguntas disponíveis.
        """
        
        self.display_available_categories()
        self.category_to_play = int(input("\nWhich of these categories do you want to play the Quiz Game?: "))
        self.number_of_questions_to_play = int(input("How many questions do you want to play?: "))
        questions_bank = self.create_question_bank()
        
        return questions_bank
