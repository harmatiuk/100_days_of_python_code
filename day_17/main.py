from generation_questions import GenerateQuestionsFromApi
from quiz_brain import QuizBrain


questions = GenerateQuestionsFromApi()
question_bank = questions.questions_generator()

if question_bank:
    game = QuizBrain(question_bank)

    while game.still_has_question():
        game.next_question()

    print("You've completed the quiz")
    print(f"Your final score was: {game.score} / {game.question_number}")
else:
    print("Sorry, we have no questions in our database about this category")