class QuizBrain:
    """
    """
    
    def __init__(self, question_list:list) -> None:
        self.question_number = 0
        self.score = 0
        self.question_list = question_list
        
    
    def still_has_question(self) -> bool:
        """Check if still has question to the user

        Returns:
            bool: True if has question and False otherwise
        """
        
        return len(self.question_list) > self.question_number
    
    def check_answer(self, user_answer: str, correct_answer:str) -> None:
        """_summary_

        Args:
            user_answer (str): _description_
            correct_answer (str): _description_
        """
        
        if user_answer.lower() == correct_answer.lower():
            self.score += 1
            print("You got it right!")
        else:
            print("That's wrong!")
        print(f"The correct anwser was: {correct_answer}")
        print(f"Your current score is: {self.score}/{self.question_number}.")
        print("\n")
    
    def next_question(self) -> input:
        """Pass to the next question

        Returns:
            input: Question of the question list
        """
        current_question = self.question_list[self.question_number]
        self.question_number +=1
        
        user_answer = input(f"Q.{self.question_number}: {current_question.text} (True/False)? : ")
        
        self.check_answer(user_answer=user_answer, correct_answer=current_question.answer)

        