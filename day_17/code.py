import requests


response = requests.get("https://opentdb.com/api_category.php").json()

categories = sorted(response['trivia_categories'], key=lambda item: item['name'])  

category_options = {n: {"api_id":category['id'], "name":category['name']} for n, category in enumerate(iterable=categories,  start=1)}


#categories_formated = [f"{n} - {category['name']}" for n, category in enumerate(iterable=categories,  start=1)]
string_display = "\n".join(f"{key} - {value['name']}" for key, value in category_options.items())

print(string_display)