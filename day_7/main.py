import random
from arts import arts
from words import words

arts_dict = arts()
word_list = words()

print(arts_dict["logo"])
chosen_word = random.choice(word_list)
display = ["_" for _ in chosen_word]
print(" ".join(map(str, display)))

end_of_the_game = False
lives = 6
game_stages = arts_dict["game_stages"]
letters_guess = list()
while not end_of_the_game:
    
    guess  = input("Guess a letter: ").lower()

    for index, letter in enumerate(chosen_word):
        if letter == guess:
            display[index] = letter
    
    if guess not in chosen_word:
        print(f"You guessed {guess}, that's not in the word. You lose a life.")
        lives -=1
        print(game_stages[lives])

    if guess in display:
        print(f"You've already guessed {guess}")
        
    print(" ".join(map(str, display)))
    
    if "_" not in display:
        end_of_the_game = True
        print("You win!")
    
    if lives == 0:
        print("You lose!")
        end_of_the_game = True
        