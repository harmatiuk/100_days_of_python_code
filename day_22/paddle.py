from turtle import Turtle

class Paddle(Turtle):
    """
    """
    
    def __init__(self, position: tuple) -> None:
        super().__init__()
        self.position = position
        self.create()
    
    
    def create(self):
        self.shape('square')
        self.color('white')
        self.shapesize(stretch_wid=5, stretch_len=1)
        self.penup()
        self.goto(self.position)
    
    def move(self, new_y):
        self.goto(self.xcor(), new_y)
        
    def move_up(self):
        new_y = self.ycor() + 20
        self.move(new_y)
    
    def move_down(self):
        new_y = self.ycor() - 20
        self.move(new_y)


        