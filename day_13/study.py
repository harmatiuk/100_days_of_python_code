class Produto:
    
    def __init__(self, nome, preco, quantidade) -> None:
        self.nome = nome
        self.preco = preco
        self.quantidade = quantidade
    
    def mostrar_info(self):
        print(f"Nome: {self.nome}")
        print(f"Preço: {self.preco}")
        print(f"Quantidade: {self.quantidade}")
    
    def mostar_valor_total_estoque(self):
        valor_total = self.preco * self.quantidade
        print(f"O valor total de estoque desde protudo é de R$:{round(valor_total,2)}")

p1 = Produto("Água", 1.99, 20)
p2 = Produto("Refrigerante", 4.99, 25)

class ProdutoPerecivel(Produto):
    
    # Adição de um novo atributo (data_validade)
    def __init__(self, nome, preco, quantidade, data_validade) -> None:
        super().__init__(nome, preco, quantidade)
        self.data_validade = data_validade
        
    # Novo método
    def mostrar_validade(self):
        print(f"O produto vence no dia {self.data_validade}")
    
    def mostrar_info(self):
        super().mostrar_info()
        print("="*30)
        print("Esse produto é perecível!")
        print("="*30)
        
p3 = ProdutoPerecivel("Leite", 7.99, 10, "10/05/2023")
p3.mostrar_info()
p3.mostar_valor_total_estoque()
p3.mostrar_validade()