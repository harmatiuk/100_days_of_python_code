from art import logo
import os

print(logo)

bids = dict()


while True:
    name = input("What's your name? \n")
    bid = float(input("Whats your bid? \n"))

    bids[name] = bid

    have_other_bidder = input("Are there any other bidders? Type 'yes' or 'no'. \n").lower()

    if have_other_bidder == "no":
        winner = max(zip(bids.values(), bids.keys()))
        print(f"The winner is {winner[1]} with a bid of R${winner[0]}")
        break
    elif have_other_bidder == 'yes':
         os.system("clear")
    else:
        print("Sorry, this is a invalid command.")
        raise ValueError