menu = {
    1: {
        "name":"Espresso",
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    2: {
        "name":"Late",
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    3: {
        "name":"Cappuccino",
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
    "money": 0
}

coins = {
   "Penny":{
      "value":0.01,
      "aka":"pennies"
   },
   "Nickel":{
      "value":0.05,
      "aka":"nickles"
   },
   "Dime":{
      "value":0.10,
      "aka":"dimes"
   },
   "Quarter":{
      "value":0.25,
      "aka":"quarters"
   }
}