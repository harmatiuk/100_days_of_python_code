from data import menu, resources, coins

def build_coin_input(coin: str):
    return input(f"   How many {coins.get(f'{coin}').get('aka')}: ")

def get_coin_value(coin):
    return coins[coin]["value"]

def calculate_process_coins() -> float:
    
    print("Please insert coin:")
    total  = int(build_coin_input('Quarter')) * 0.25
    total += int(build_coin_input('Dime')) * 0.1
    total += int(build_coin_input('Nickel')) * 0.05
    total += int(build_coin_input('Penny')) * 0.01
    return round(total, 2)

def report():
    print("\n")
    print("*************************************")
    print(f"Water: {resources.get('water')}ml")
    print(f"Milk: {resources.get('milk')}ml")
    print(f"Coffee: {resources.get('coffee')}g")
    print(f"Money: R${resources.get('money')}")
    print("*************************************")

def update_resources(order_infos):
    for item in order_infos['ingredients']:
        resources[item] -= order_infos['ingredients'][item]
    resources['money'] +=  order_infos['cost']
    
    return report()
    

def is_resources_sufficient(order_ingredients):
    for item in order_ingredients:
        if order_ingredients[item] >= resources[item]:
            print(f"Sorry there is not enough {order_ingredients[item]}")
            return False
    return True

while True:
    user_coffe = int(input("What would you like ? \n\nPress:\n1 - Espresso\n2 - Late\n3 - Cappuccino\n4 - Report\n5 - Turn OFF\n\nselected: "))

    if user_coffe == 5:
        break
    elif user_coffe == 4:
        report()
    else:
        drink = menu.get(user_coffe)
        if is_resources_sufficient(drink['ingredients']):
            payment = calculate_process_coins()
            if payment >= menu.get(user_coffe).get("cost"):
                change = payment - menu.get(user_coffe).get("cost")
                if change > 0:
                    print(f"Here is R${change} in change")
                update_resources(drink)
                print(f"Here is your {menu.get(user_coffe).get('name')} ☕️. Enjoy!")
            else:
                print("Sorry that's not enough money. Money refounded.")

            