class MenuItem:
    
    def __init__(self, name:str, cost:float, ingredients: dict):
        self.name = name,
        self.cost = cost,
        self.ingredients = ingredients

class Menu:
    
    def __init__(self, order_id:int):
        self.order_id = order_id
    
    def get_items():
        pass
    
    def find_drink():
        pass


class CoffeMaker:
    
    def __init__(self) -> None:
        pass
    
    def report():
        pass
    
    def is_resources_sufficient(order_id):
        pass
    
    
    def make_coffe(order_id):
        pass
    
class MoneyMachine:
    
    def __init__(self) -> None:
        pass
    
    def report():
        pass

    def make_payment(cost):
        pass
    

class ResourcesControl:

    def __init__(self, order_details:dict) -> None:
        self.resources = dict(
            water = 300,
            milk  = 200,
            coffe = 100,
            money = 0
        ),
        self.order_details = order_details
    
    def update_resources(self):
        for item in self.order_details['ingredients']:
            self.resources[item] -= self.order_details['ingredients'][item]
        self.resources['money'] +=  self.order_details['cost']
       
    def report_show(self):
        report = MakeReport(self.resources)
        report.show()
    
class MakeReport:
    
    def __init__(self, resources:dict) -> None:
        self.resources = resources
    
    def show(self):
        print("\n")
        print("*************************************")
        print(f"Water: {self.resources.get('water')}ml")
        print(f"Milk: {self.resources.get('milk')}ml")
        print(f"Coffee: {self.resources.get('coffee')}g")
        print(f"Money: R${self.resources.get('money')}")
        print("*************************************")
    
    