from resources_control import ResourcesControl
from menu import Menu
from money_control import MoneyMachine

menu = Menu()
resources = ResourcesControl()
money = MoneyMachine()

print("Report Start: ")
resources.report_show()
money.report_show()

while True:
    user_coffee = menu.get_user_coffee()
    if user_coffee == 5:
        break
    elif user_coffee == 4:
        print("Report: ")
        resources.report_show()
        money.report_show()
    else:
        order_details = menu.get_order_details(user_coffee)
        if resources.is_resources_sufficient(order_details["ingredients"]):
            payment = money.calculate_process_coins()
            if payment >= order_details["cost"]:
                change = payment - order_details["cost"]
                if change > 0:
                    print(f"Here is R${round(change,2)} in change")
                resources.update_resources(order_details)
                money.update_money(order_details)
                print(f"Here is your {order_details.get('name')} ☕️. Enjoy!")
            else:
                print("Sorry that's not enough money. Money refounded.")
        else:
            break
