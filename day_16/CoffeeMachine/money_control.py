from make_report import MakeReport

class MoneyMachine():

    def __init__(self) -> None:
        self.money = dict(
            total = 0.0
        )
        self.coins = dict(
            Penny= dict(
               value = 0.01,
               aka   = 'pennies'
            ),
            Nickel= dict(
               value = 0.05,
               aka   = 'nickles'
            ),
            Dime = dict(
               value = 0.10,
               aka   = 'dimes'
            ),
            Quarter=dict(
               value = 0.25,
               aka   = 'quarters'
            )
        )
    
    def build_coin_input(self, coin:str):
        return input(f"   How many {self.coins.get(f'{coin}').get('aka')}: ")
    
    def calculate_process_coins(self) -> float:

        print("Please insert coin:")
        total  = int(self.build_coin_input('Quarter')) * 0.25
        total += int(self.build_coin_input('Dime')) * 0.1
        total += int(self.build_coin_input('Nickel')) * 0.05
        total += int(self.build_coin_input('Penny')) * 0.01
        return round(total, 2)
    
    def update_money(self, order_details) -> dict:
        """
        Update money based on the user coffee ingredients

        Args:
            order_details (dict): details of user order (name of coffee, ingredients and cost)

        Returns:
            dict: updated money of the machine
        """
        payment = order_details.get('cost')
        self.money["total"] += payment
        return self.money["total"] 
    
    def report_show(self) -> None:
        """
        Print report for user
        """               
        report = MakeReport(self.money)
        report.show()
    