from make_report import MakeReport

class ResourcesControl:

    def __init__(self) -> None:
        self.resources = dict(
            water = 300,
            milk  = 200,
            coffee = 100,
        )
        
    def is_resources_sufficient(self, order_ingredients):
        """
        Check if is resources sufficient for user order.

        Args:
            order_ingredients (_type_): ingredients of user order.

        Returns:
            bool: True if is resources sufficient and False if is not.
        """
        for item in order_ingredients:
            if order_ingredients[item] > self.resources[item]:
                print(f"Sorry there is not enough of {item}")
                return False
        return True 

    def update_resources(self, order_details:dict) -> dict:
        """
        Update resources based on the user coffee ingredients

        Args:
            order_details (dict): details of user order (name of coffee, ingredients and cost)

        Returns:
            dict: updated resources of the machine
        """
        if self.is_resources_sufficient(order_details.get('ingredients')):
            for item in order_details['ingredients']:
                self.resources[item] -= order_details['ingredients'][item]
            return self.resources 
       
    def report_show(self) -> None:
        """
        Print report for user
        """               
        report = MakeReport(self.resources)
        report.show()