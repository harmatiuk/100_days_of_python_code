class MakeReport:
    """
    Generate layout for user report
    """
    
    def __init__(self, resources:dict) -> None:
        self.resources = resources
    
    def show(self) -> None:
        """
        Show report for user
        """
        from prettytable import PrettyTable
         
        table = PrettyTable()
        table.field_names = self.resources.keys()
        table.add_row(self.resources.values())
        print(table, '\n')
        
        