class Menu:
    
    def __init__(self) -> None:
        self.menu = {
            1: {
                "name":"Espresso",
                "ingredients": {
                    "water": 50,
                    "coffee": 18,
                },
                "cost": 1.5,
            },
            2: {
                "name":"Late",
                "ingredients": {
                    "water": 200,
                    "milk": 150,
                    "coffee": 24,
                },
                "cost": 2.5,
            },
            3: {
                "name":"Cappuccino",
                "ingredients": {
                    "water": 250,
                    "milk": 100,
                    "coffee": 24,
                },
                "cost": 3.0,
            }
    }
        
    def build_display_menu(self) -> str:
        """_summary_

        Returns:
            str: _description_
        """
        max_menu_key = max(self.menu.keys())
        additional_options = [
            f"{max_menu_key + 1} - Report",
            f"{max_menu_key + 2} - Turn OFF"
        ]
        menu_items = [f"{key} - {value['name']}" for key, value in self.menu.items()]
        
        string = 'What would you like ?\n'
        string += "\n".join(menu_items + additional_options)
        string += "\n\nselected: "
        
        return string
    
    def get_user_coffee(self) -> int:
        """_summary_

        Returns:
            int: _description_
        """
        user_choice = int(input(self.build_display_menu()))
        return user_choice
    
    def get_order_details(self, user_coffee) -> dict:
        return self.menu.get(user_coffee)