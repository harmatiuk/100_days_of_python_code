import random
from game_parameters import game_parameters

game_parameters = game_parameters
user_choice     = str(input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors.\n"))
compute_choice   = str(random.randint(0,2))

try:
    
    print("User choise:\n",
          game_parameters.get(user_choice).get("name"),
          game_parameters.get(user_choice).get("output")
          )
    print("Compute choise:\n",
          game_parameters.get(compute_choice).get("name"),
          game_parameters.get(compute_choice).get("output")
          )

    if user_choice == compute_choice:
        print("Draw in the game!")
    elif compute_choice in game_parameters.get(user_choice).get("lose_to"):
        print("You Lose!")
    else:
        print("You Win!")
        
except:
    print(f"You typed an invalid number. You lose!")
    