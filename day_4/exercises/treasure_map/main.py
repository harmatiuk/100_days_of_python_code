
#         a     b   c
line1 = ["⬜️","⬜️","️⬜️"]
line2 = ["⬜️","⬜️","️⬜️"]
line3 = ["⬜️","⬜️","⬜️"]

map = [line1, line2, line3]

print("Hiding your treasure! X marks the spot.")

position = input("Where do you want to put the treasure? \n")

coordinates = dict(
    a1 = (0,0), 
    a2 = (0,1),
    a3 = (0,2),
    
    b1 = (1,0),
    b2 = (1,1),
    b3 = (1,2),
    
    c1 = (2,0),
    c2 = (2,1),
    c3 = (2,2),
)

coordinate = coordinates.get(position.lower())
map[coordinate[1]][coordinate[0]]= "X"

print(f"{map[0]}\n{map[1]}\n{map[2]}") 