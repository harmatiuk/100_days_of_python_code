from turtle import Turtle, colormode
from random import choices

turtle = Turtle()
turtle.pensize(1)
colormode(255)


directions: list[int] = [0,90,180,270]
turtle.speed('fastest')


for _ in range(100):
    turtle.color(*choices(range(0,256), k=3))
    turtle.circle(100)
    turtle.setheading(turtle.heading() + 10)



