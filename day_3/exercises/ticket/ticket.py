print("Welcome to the rollercoaster!")
height = int(input("What is your height in cm? \n"))
ticket = 0

parameters = dict(
    min_height = 120,
    ticket_values = dict(
        child = 5,
        youth = 7,
        adult = 12,
        olde  = 0
    ),
    age_logic = dict(
        child_max = 12,
        youth_max = 18,
        adult_max = 45
    ),
    photos_value = 3
)

def message(classification, ticket_value):
    return print(f"{classification} tickets ${ticket_value}!")


if height > parameters["min_height"]:
    print("You can ride the rollercoaster!")
    age = int(input("What is your age? \n"))
    
    if age < parameters["age_logic"].get("child_max"):
        value = parameters["ticket_values"].get("child")
        ticket += value
        message("child", value)
    elif age <= parameters["age_logic"].get("youth_max"):
        value = parameters["ticket_values"].get("youth")
        ticket += value
        message("youth", value)
    elif age > parameters["age_logic"].get("youth_max") and age < parameters["age_logic"].get("adult_max"):
        value = parameters["ticket_values"].get("adult")
        ticket += value
        message("adult", value)
    elif age >= parameters["age_logic"].get("adult_max"):
        value = parameters["ticket_values"].get("adult")
        ticket += parameters["ticket_values"].get("old")
        message("old", ticket)
    
    want_photos = input("Do you wnat photos? Y or N \n")
    if want_photos.lower() == "y":
        ticket += parameters.get("photos_value")
        
print(ticket)
        
    
         
    
 

    