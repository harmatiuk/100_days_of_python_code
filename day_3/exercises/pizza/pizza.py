print("Thank you for choosing Python Pizza Deliveries!")
size          = input("What size pizza do you want? S, M or L \n")
add_pepperoni = input("Do you want pepperoni? Y or N \n")
extra_cheese  = input("Do you want extra cheese? Y or N \n")

pizza_values = dict(
    s = 15,
    m = 20,
    l = 25
)

add_pepperoni_values = dict(
    s = 2,
    m = 3,
    l = 3
)

add_extra_chesse_values = dict(
    s = 1,
    m = 1,
    l = 1
)

bill = dict(
    pizza_value            = 0,
    add_pepperoni_value    = 0,
    add_extra_chesse_value = 0
)

size = size.lower()

bill.update({"pizza_value" : pizza_values.get(size, 0)})

if add_pepperoni.lower() == "y":
    bill.update({"add_pepperoni_value" : add_pepperoni_values.get(size, 0)})
    
if extra_cheese.lower() == "y":
    bill.update({"add_extra_chesse_value" : add_extra_chesse_values.get(size, 0)})

total_bill = bill["pizza_value"] + bill["add_pepperoni_value"] + bill["add_extra_chesse_value"]

print("Thank you for choosing Python Pizza Deliveries!")
print(f"Your final bill is: ${total_bill}")
        
        
