print("Welcome to the calculator love!")

your_name  = input("What's your name?")
their_name = input("What's their name?")

combine_names = f"{your_name.lower()}{their_name.lower()}"

phrase_one = "true"
phrase_two = "love"

score_one   = 0
score_two   = 0
score_final = 0

for letter in phrase_one:
    score_one += combine_names.count(letter)

for letter in phrase_two:
    score_two += combine_names.count(letter)

score_final += int(f"{score_one}{score_two}")

if score_final <= 10 and score_final >= 90:
    print(f"Your score is {score_final}, you go together like coke and mentos.")
elif score_final >= 40 and score_final <= 50:
    print(f"Your score is {score_final}, you are alright together.")
else:
    print(f"Your score is {score_final}.")