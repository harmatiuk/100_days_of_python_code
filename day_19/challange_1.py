from turtle import Turtle, Screen

tim = Turtle()
screen = Screen()


def move_forwards():
    tim.forward(10)
    
def move_backward():
    tim.backward(10)
    
def move_left():
    new_headding = tim.heading() + 10
    tim.setheading(new_headding)

def move_right():
    new_headding = tim.heading() - 10
    tim.setheading(new_headding)

def clear():
    tim.clear()
    tim.penup()
    tim.home() 
    tim.pendown()   

screen.listen()

commands = dict(
    w = move_forwards,
    s = move_backward,
    a = move_left,
    d = move_right,
    Up = move_forwards,
    Down = move_backward,
    Left = move_left,
    Right = move_right,
    c = clear
)

for key, fun in commands.items():
    screen.onkey(key=key, fun=fun)

screen.exitonclick()