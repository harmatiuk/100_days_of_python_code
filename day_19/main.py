from turtle import Turtle, Screen
import random

screen = Screen()
screen.setup(width=500, height=400)
user_bet = screen.textinput(title='Make yout bet', prompt="Which turttle will win the race? Enter a colo:")

colors: list[str] = ['red', 'orange', 'black', 'green', 'blue', 'purple']
inicial_positions: list[int] = [-70, -40, -10, 20, 50, 80]

all_turtles: list[Turtle] = []

for i in range(0, 6):
    turtle = Turtle(shape='turtle')
    turtle.color(colors[i])
    turtle.penup()
    turtle.setposition(x=-230, y=inicial_positions[i])
    all_turtles.append(turtle)


is_race_on = False
if user_bet:
    is_race_on = True

def check_bet(user_bet, winning_color):
    if winning_color == user_bet:
        print(f"You've won! The {winning_color} turtle is the winner!")
    else:
        print(f"You've lost! The {winning_color} turtle is the winner!")

while is_race_on:
    for turtle in all_turtles:
        if turtle.xcor() > 230:
            is_race_on = False
            winning_color = turtle.pencolor()
            check_bet(user_bet, winning_color)             
        random_distance = random.randint(0,10)
        turtle.forward(random_distance)

screen.exitonclick()