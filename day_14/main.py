from art import logo, vs
from utils import get_random_data, show_infos, check_answer
import os


print(logo, "\n")

score = 0
account_a = get_random_data()
account_b = get_random_data()

while True:
    account_a = account_b
    account_b = get_random_data()
    
    while account_a == account_b:
        account_b = get_random_data()
        
      
    print(f"Compare A: {show_infos(account_a)}")
    print(vs,"\n")
    print(f"Compare B: {show_infos(account_b)}")
    
    user_input = input("Who has more followers? Type 'A' or 'B': ").lower()
    
    a_follower_count = account_a.get("follower_count")
    b_follower_count = account_a.get("follower_count")
    
    is_corret = check_answer(user_answer=user_input, a_followers=a_follower_count,
                             b_followers=b_follower_count)
    
    os.system("clear")
    
    print(logo)
    
    if is_corret:
        score+=1
        print(f"You're right! Current Score: {score}.")
    else:
        print(f"Sorry, that's wrong. Final Score: {score}.")
        break
        