from game_data import data
import random

def get_random_data() -> dict:
    """Get data from random acccount
    """
    return random.choice(data)

def show_infos(account:dict) -> str:
    """Format account into printable format:
    name, description and country.

    Args:
        account (dict): dictionary with account infos.

    Returns:
        str: printable format text
    """    
    name = account.get('name', '')
    description = account.get('description')
    country = account.get('country')
    
    text = f"{name}, a {description}, from {country}"
    return text

def check_answer(user_answer:str, a_followers:int, b_followers:int) -> bool:
    """Checks followers against user's anwswer 
    and returns True if they got it right.
    Or False if they got it wrong.

    Args:
        user_answer (str): user answer (a or b)
        a_followers (int): quantity of account followers
        b_followers (int): quantity of account followers

    Returns:
        bool: True if user answer is corret or False if user answer is wrong.
    """
    if a_followers > b_followers:
        return user_answer == "a"
    else:
        return user_answer == "b"