from art import arts

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


arts()

def caesar(text:str, shift_amount:int, encode_or_decode:str) -> str:
    
    output_message = ""
    
    if encode_or_decode == 'decode':
        shift_amount *= -1
   
    for character in text:
        if character in alphabet:
            position = alphabet.index(character)        
            new_position = (position + shift_amount) % 26   
            output_message += alphabet[new_position]
        else:
            output_message += character
        
    print(f"Here's the {encode_or_decode} result: {output_message}")


while True:
    
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))

    shift = shift % 26
    
    caesar(text=text, shift_amount=shift, encode_or_decode=direction)
    
    restart = input("Type 'yes' if you want to encode/decode again. Otherwise type 'no'. \n")
    if restart == "no":
        print("Goodbye!")
        break