import random
from art import logo

print(logo)

print("Welcome to the Number Guessing Game!")
print("I'm thinking of a number between 1 and 100.")
number = random.randint(0,100)
print(f"Pssst, the correct answer is {number}")


def check_answer(user_input:int, number:int) -> bool:
    """
    Compare user_input with number and return True or False.
    If user_input more than number then Too High.
    If user_input less than number then Too Low.
    Else got the number right

    Args:
        user_input (int): user number
        number (int): random number

    Returns:
        bool: True or False
    """
    if user_input > number:
        print("Too High.")
        return False
    elif user_input < number:
        print("Too Low.")
        return False
    else:
        print(f"You got it! The answer was {number}.")
        return True

dificulty_level = input("Choose a difficulty. Type 'easy' or 'hard': ").lower()
game_args  = dict(
    
    life = dict(
        easy = 10,
        hard = 5  
    )
)
lifes = game_args["life"].get(dificulty_level, 0)

while True:
    print(f"You have {lifes} attempts remaining to guess the number.")
    user_input = int(input("Make a guess: "))
    lifes -= 1
    if check_answer(user_input=user_input, number=number):
        break
    elif lifes == 0:
        print("You don't have more lifes ;/ \nYou lose!")
        break
        