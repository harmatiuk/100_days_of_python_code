import random

def deal_card() -> int:
    """
    Return a card of the deck cards.

    Returns:
        int: card
    """
    
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    card = random.choice(cards)
    return card

def calculate_score(cards: list) -> int:
    """
    Take a list of cards and return the score calculated from the cards.

    Args:
        cards (list): list of cards

    Returns:
        int: score of the cards
    """
    
    score = sum(cards)
    
    if len(cards) == 2 and score == 21:
        return 0
    else:
        if score > 21 and 11 in cards:
            cards.remove(11)
            cards.append(1)
            return sum(cards)
    return score

def compare_scores(player_score: int, computer_score: int) -> str:
    """
    Compare player and compute score and return a string value.
    If the computer and user both have the same score, then it's a draw. 
    If the computer has a blackjack (0), then the user loses. 
    If the user has a blackjack (0), then the user wins. 
    If the user_score is over 21, then the user loses. 
    If the computer_score is over 21, then the computer loses. 
    If none of the above, then the player with the highest score wins.

    Args:
        player_score (int): player score
        computer_score (int): compute score

    Returns:
        str: result of comparation
    """
    
    if player_score == computer_score:
        return "    Draw 🙃"
    elif computer_score == 0:
        return "    Lose, opponent has Blackjack 😱"
    elif player_score == 0:
        return "    Win with a Blackjack 😎"
    elif player_score > 21:
        return  "   You went over. You lose 😭"
    elif computer_score > 21:
        return "    Opponent went over. You win 😁"
    elif player_score > computer_score:
        return "    You win 😃"
    else:
        return "    You lose 😤"
        