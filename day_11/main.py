from art import logo
from utils import deal_card, calculate_score, compare_scores
import os

def play_game():
    print(logo)

    player_cards = list()
    computer_cards = list()

    player_score = 0
    computer_score = 0

    for _ in range(2):
        player_cards.append(deal_card())
        computer_cards.append(deal_card())

    while True:    
        player_score = calculate_score(player_cards)
        computer_score = calculate_score(computer_cards)

        print(f"    Your cards: {player_cards}, current score: {player_score}")
        print(f"    Computer first card: {computer_cards[0]} \n")

        if computer_score == 0 or player_score == 0 or player_score > 21 or player_score == 21:
            break
        else:
            another_card = input("    Type 'y'to get another card, type 'n' to pass: ").lower()
            if another_card == "y":
                player_cards.append(deal_card())
            else:
                break
            
    while computer_score != 0 and computer_score < 17:
        computer_cards.append(deal_card())
        computer_score = calculate_score(computer_cards)

    print(f"    Your cards: {player_cards}, current score: {player_score}")
    print(f"    Computer cards: {computer_cards}, current score: {computer_score}")
    print(compare_scores(player_score=player_score, computer_score=computer_score), "\n")
    
while input("   Do you want to play a game of Blackjack? Type 'y' or 'n': ") == "y":
    os.system("clear")
    play_game()