from art import logo

def add (n1:int, n2:int) -> float:
    return n1 + n2

def sub (n1:int, n2:int) -> float:
    return n1 - n2

def div (n1:int, n2:int) -> float:
    return n1 / n2

def mult (n1:int, n2:int) -> float:
    return n1 * n2


operations = {
    "+": add,
    "-": sub,
    "/": div,
    "*": mult
}

def calculator():
    
    print(logo)
    
    n1 = float(input("What's the first number?: "))
    for operation in operations:
        print(operation)
    operation = str(input(f"What's the operation?: "))

    while True:
        n2 = float(input("What's the next number?: "))

        result = operations[operation](n1=n1, n2=n2)

        print(f"{n1} {operation} {n2} = {result}")

        _continue = str(input(f"Type 'y' o continue calculating with {result}, or type 'n' o exit:")).lower()

        if _continue == 'y':
            n1 = result   
        else:    
            calculator()
            break
            
        
calculator()