from turtle import Turtle

ALIGNMENT = "center"
FONT = ('Arial', 24, 'normal')

class ScoreBoard(Turtle):
    """
    """
    
    def __init__(self) -> None:
        super().__init__()
        self.score: int = 0
        self.color('white')
        self.penup()
        self.goto(0,270)
        self.hideturtle()
        self.display()
    
    def display(self) -> None:
        """
        """
        self.write(arg=f'Score: {self.score}', move=False, align=ALIGNMENT, font=FONT)
        
    def refresh(self) -> None:
        """
        """
        self.score += 1
        self.clear()
        self.display()
    
    def game_over(self):
        """
        """
        self.goto(0,0)
        self.write(arg='GAME OVER', move=False, align=ALIGNMENT, font=FONT)
        
        