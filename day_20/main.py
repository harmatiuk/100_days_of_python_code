from turtle import Screen
from snake import Snake
from food import Food
from scoreboard import ScoreBoard
import time


screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor('black')
screen.title("Snake Game")
screen.tracer(0)

snake = Snake()
food = Food()
score = ScoreBoard()

screen.listen()
screen.onkey(snake.up, "Up")
screen.onkey(snake.down, "Down")
screen.onkey(snake.left, "Left")
screen.onkey(snake.right, "Right")

game_is_on = True
while game_is_on:
    screen.update()
    time.sleep(0.1)
    snake.move()
    
    # Detect colission with food
    if snake.head.distance(food) < 15:
        food.refresh_position()
        snake.extend()
        score.refresh()
        
    
    # Detect colission with wall
    x, y = abs(snake.head.xcor()), abs(snake.head.ycor())
    if x > 280 or y > 280:
        game_is_on = False
        score.game_over()
    
    # Detect colission with tail
    for piece_snake in snake.snake:
        if snake.head == piece_snake:
            continue
        if snake.head.distance(piece_snake) < 10:
            game_is_on = False
            score.game_over()
            
    

screen.exitonclick()