from turtle import Turtle, Screen

tim = Turtle()
screen = Screen()


def move_up():
    tim.setheading(to_angle=90)
    tim.forward(1)
    
def move_down():
    tim.setheading(to_angle=270)
    tim.forward(1)
    
def move_left():
    tim.setheading(to_angle=180)
    tim.forward(1)
    
def move_right():
   tim.setheading(to_angle=0)
   tim.forward(1)

screen.listen()

commands = dict(
    Up    = move_up,
    Down  = move_down,
    Left  = move_left,
    Right = move_right
)

for key, fun in commands.items():
    screen.onkey(key=key, fun=fun)

screen.exitonclick()