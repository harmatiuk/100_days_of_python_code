from turtle import Turtle

START_POSITIONS: list[tuple] = [(0,0), (-20,0), (-40,0)]
MOVE_DISTANCE: int = 20
DIRECTIONS = dict(
    UP    = 90,
    DOWN  = 270,
    LEFT  = 180,
    RIGHT = 0)

class Snake:
    """
    Represents a snake in the game.
    """
    
    def __init__(self) -> None:
        """
        Initializes the snake object.
        Creates the initial snake segments and sets the head direction
        """
        self.snake = list()
        self.create()
        self.head = self.snake[0]

        
    def create(self) -> None:
        """
        Creates the initial snake segments.
        """
        for position in START_POSITIONS:
            self.add_pieces(position)

    def add_pieces(self, position):
        """"""
        new_segment = Turtle(shape='square')
        new_segment.color('white')
        new_segment.penup()
        new_segment.goto(position)
        self.snake.append(new_segment)
    
    def extend(self):
        self.add_pieces(self.snake[-1].position())
    
    def move(self) -> None:
        """
        Moves the snake
        """
        for piece_snake in range(len(self.snake) - 1, 0, -1):
            new_x = self.snake[piece_snake - 1].xcor()
            new_y = self.snake[piece_snake - 1].ycor()
            self.snake[piece_snake].goto(new_x, new_y)
        self.head.forward(MOVE_DISTANCE)
    
    def up(self):
        """
        Sets the snake's direction to up
        """
        if self.head.heading() != DIRECTIONS.get('DOWN'):
            self.head.setheading(to_angle=90)
        
    def down(self):
        """
        Sets the snake's direction to down
        """
        if self.head.heading() != DIRECTIONS.get('UP'):
            self.head.setheading(to_angle=270)
        
    def left(self):
        """
        Sets the snake's direction to left
        """
        if self.head.heading() != DIRECTIONS.get('RIGHT'):
            self.head.setheading(to_angle=180)
        
    def right(self):
       """
       Sets the snake's direction to right
       """
       if self.head.heading() != DIRECTIONS.get('LEFT'):
           self.head.setheading(to_angle=0)
